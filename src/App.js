import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./App.css";

const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^[A-Za-z ]*$/, "Only alphabet characters and spaces are allowed.")
    .max(100, "First name is too long.")
    .required("First name is required."),
  lastName: Yup.string()
    .matches(/^[A-Za-z ]*$/, "Only alphabet characters and spaces are allowed.")
    .max(100, "Last name is too long.")
    .required("Last name is required."),
  age: Yup.number()
    .integer("Age must be a whole number.")
    .min(16, "Minimum age is 16.")
    .max(65, "Maximum age is 65.")
    .required("Age is required."),
  notes: Yup.string().max(100, "Notes cannot exceed 100 characters."),
});

const initialValues = {
  firstName: "",
  lastName: "",
  age: "",
  employed: false,
  color: "",
  sauces: [],
  stooge: "Larry",
  notes: "",
};

const onSubmit = (values) => {
  alert(JSON.stringify(values, null, 2));
};

function App() {
  return (
    <div className="App">
      <h1>User Form</h1>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ isSubmitting, dirty, values }) => (
          <Form>
            <div className="form-control">
              <label className="form-seperate" htmlFor="firstName">First Name</label>
              <Field type="text" name="firstName" />
              <ErrorMessage
                name="firstName"
                component="div"
                className="error"
              />
            </div>

            <div className="form-control">
              <label className="form-seperate" htmlFor="lastName">Last Name</label>
              <Field type="text" name="lastName" />
              <ErrorMessage name="lastName" component="div" className="error" />
            </div>

            <div className="form-control">
              <label className="form-seperate" htmlFor="age">Age</label>
              <Field type="number" name="age" />
              <ErrorMessage name="age" component="div" className="error" />
            </div>

            <div className="form-control">
              <label>
                <Field type="checkbox" name="employed" />
                Employed
              </label>
            </div>

            <div className="form-control">
              <label className="form-seperate" htmlFor="color">Favorite Color</label>
              <Field type="color" name="color" />
            </div>

            <div className="form-control">
              <h4 className="form-Sauces">Sauces</h4>
              <div className="form-column">
                <label>
                  <Field type="checkbox" name="sauces" value="ketchup" />
                  Ketchup
                </label>
                <label>
                  <Field type="checkbox" name="sauces" value="mayonnaise" />
                  Mayonnaise
                </label>
                <label>
                  <Field type="checkbox" name="sauces" value="guacamole" />
                  Guacamole
                </label>
                <label>
                  <Field type="checkbox" name="sauces" value="mustard" />
                  Mustard
                </label>
              </div>
            </div>

            <div className="form-control">
              <h4 className="form-Sauces" >Best Stooge</h4>
              <div className="form-column"> <label>
                <Field type="radio" name="stooge" value="Larry" />
                Larry
              </label>
              <label>
                <Field type="radio" name="stooge" value="Moe" />
                Moe
              </label>
              <label>
                <Field type="radio" name="stooge" value="Curly" />
                Curly
              </label></div>
             
            </div>

            <div className="form-wrap">
              <label htmlFor="notes">Notes (Max 100 characters)</label>
              <Field as="textarea" name="notes" maxLength="100" />
              <ErrorMessage name="notes" component="div" className="error" />
            </div>

            <div className="form-center form-control">
              <button type="submit" disabled={isSubmitting || !dirty}>
                Submit
              </button>
              <button type="reset" disabled={!dirty}>
                Reset
              </button>
            </div>
            <div className="form-pre">
              <pre>{JSON.stringify(values, null, 4)}</pre>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default App;
